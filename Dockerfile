FROM python:3.9-slim

RUN apt update &&\
    apt install -y \
    bluez \
    make \
    gcc \
    libglib2.0-dev \
    --no-install-recommends  &&\
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt

RUN pip install -r /requirements.txt

COPY waveplus_exporter.py /waveplus_exporter.py
